<?php

class Matrix
{
    private $_matrix = [];
    private $_matrixLength = 9;

    public function getMatrix()
    {
        return $this->_matrix;
    }

    public function createMatrix()
    {
        $this->_matrix[] = rand(0, 999999);

        if (count($this->_matrix) < $this->_matrixLength)
            return $this->createMatrix();

        return $this;
    }

}

$matrix = new Matrix();
$data = $matrix->createMatrix()->getMatrix();
print_r($data);
